import org.apache.spark.sql.SparkSession;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.io.File;
import java.sql.*;

public class HiveConnectionSpark {
    public static void main(String[] args) throws Exception{
        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark Hive Example")
                .config("spark.sql.warehouse.dir", warehouseLocation)
                .enableHiveSupport()
                .getOrCreate();
        spark.sql("use adms");
        spark.sql("select count(*) from transit_metro_bus_data");
    }
}
