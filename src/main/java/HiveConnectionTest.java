import org.apache.hadoop.security.UserGroupInformation;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class HiveConnectionTest {
    public static void main(String[] args) throws Exception{
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        conf.set("hadoop.security.authentication", "Kerberos");

        try {
            UserGroupInformation.setConfiguration(conf);
            UserGroupInformation.loginUserFromKeytab("chaoyang.he@IMSC.USC.EDU", "./chaoyang.keytab");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        String JDBC_DB_URL="jdbc:hive2://dsicloud1.usc.edu:10000/default;principal=hive/dsicloud1.usc.edu@IMSC.USC.EDU?transportMode=http;httpPath=cliservice;auth=kerberos;sasl.qop=auth-int";
        Connection con = null;
        try {
            Class.forName("org.apache.hive.jdbc.HiveDriver");
            con = DriverManager.getConnection(JDBC_DB_URL);
        } catch (SQLException e){
            e.printStackTrace();
        }

        Statement stmt = con.createStatement();
        stmt.execute("use adms;");
        ResultSet res = stmt.executeQuery("select * from transit_metro_bus_data limit 1;");
        if(res.next()){
            System.out.println(res.getString(1));
        }
    }
}
